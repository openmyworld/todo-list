import {
  CREATE_TODO,
  REMOVE_TODO,
  COMPLETE_TODO,
  EDIT_TODO,
  VISIBILITY_FILTER,
} from "./action";

export const todosReducer = (state = [], action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_TODO: {
      const { text } = payload;
      const newTodo = {
        id: Date.now(),
        text,
        createdAt: new Date().getTime(),
        isCompleted: false,
      };
      return state.concat(newTodo);
    }

    case REMOVE_TODO: {
      const { id } = payload;
      return state.filter((todoPer) => todoPer.id !== id);
    }

    case COMPLETE_TODO: {
      const { id } = payload;
      return state.map((todoPer) => {
        if (todoPer.id === id) {
          return { ...todoPer, isCompleted: true };
        } else {
          return todoPer;
        }
      });
    }

    case EDIT_TODO: {
      const { id, text } = payload;
      return state.map((todoPer) => {
        if (todoPer.id === id) {
          return { ...todoPer, text };
        } else {
          return todoPer;
        }
      });
    }

    default:
      return state;
  }
};

export const filterReducer = (state = "all", action) => {
  const { type, payload } = action;
  switch (type) {
    case VISIBILITY_FILTER:
      const { filters } = payload;
      return filters;
    default:
      return state;
  }
};
