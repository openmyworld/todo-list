import React from 'react';
import NewTodoForm from './NewTodoForm';
import FilterClick from './FilterClick';
import TodoList from './TodoList';
import styled from 'styled-components/macro';

const TodoWrapper = styled.div`
  width: 70%;
  margin: auto;

  @media (max-width: 767px) {
    width: 90%;
  }
`;

const TodoApp = () => (
  <TodoWrapper>
    <NewTodoForm />
    <FilterClick />
    <TodoList />
  </TodoWrapper>
);

export default TodoApp;
