import React from 'react';
import styled, { css } from 'styled-components/macro';
import { connect } from 'react-redux';
import { visibilityFilter } from '../redux/action';
import {
  getCompletedTodos,
  getIncompleteTodos,
  getTodos,
  getFilters,
} from '../selectors/selectors';

const FilterList = styled.ul`
  list-style-type: none;
  padding-left: 0;
  display: flex;
`;

const FilterItem = styled.li`
  width: calc(100% / 3);
`;

const FilterLink = styled.button`
  width: 100%;
  border: 0;
  padding: 10px;
  font-size: 16px;
  outline: none;

  ${(props) =>
    props.name === props.$checkFilter &&
    css`
      background-color: #4b0082;
      color: #fffff0;
      pointer-events: none;
    `}

  ${(props) =>
    props.name !== props.$checkFilter &&
    css`
      background-color: #ee82ee;
      cursor: pointer;
      opacity: 0.5;
      color: #696969;

      &:hover {
        background-color: #9400d3;
        color: #ffffff;
        opacity: 1;
      }
    `}
`;

const FilterClick = ({
  allTodos = [],
  completedTodos = [],
  inCompleteTodos = [],
  filterTodo,
  onFilter,
}) => (
  <FilterList>
    <FilterItem>
      <FilterLink
        name='all'
        $checkFilter={filterTodo}
        onClick={() => onFilter('all')}
      >
        All ({allTodos.length})
      </FilterLink>
    </FilterItem>
    <FilterItem>
      <FilterLink
        name='active'
        $checkFilter={filterTodo}
        onClick={() => onFilter('active')}
      >
        Active ({inCompleteTodos.length})
      </FilterLink>
    </FilterItem>
    <FilterItem>
      <FilterLink
        name='completed'
        $checkFilter={filterTodo}
        onClick={() => onFilter('completed')}
      >
        Done ({completedTodos.length})
      </FilterLink>
    </FilterItem>
  </FilterList>
);

const mapStateToProps = (state) => ({
  allTodos: getTodos(state),
  completedTodos: getCompletedTodos(state),
  inCompleteTodos: getIncompleteTodos(state),
  filterTodo: getFilters(state),
});

const mapDispatchToProps = {
  onFilter: visibilityFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterClick);
