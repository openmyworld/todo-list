import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createTodo } from '../redux/action';
import { getTodos } from '../selectors/selectors';
import styled from 'styled-components/macro';

const CreateTodoForm = styled.form`
  border-radius: 8px;
  padding: 16px;
  display: flex;
  box-shadow: 0 4px 8px grey;

  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

const InputContainer = styled.div`
  position: relative;
  width: 80%;

  @media (max-width: 767px) {
    width: auto;
  }
`;

const BorderBottom = styled.span`
  content: '';
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  width: 0;
  height: 2px;
  margin: 0 auto;
  background-color: rgba(0, 0, 0, 0.2);
  z-index: 2;
  transition: all 0.5s ease;
`;

const InputTodo = styled.input`
  font-size: 16px;
  padding: 6px;
  border: none;
  border-bottom: 2px solid #ddd;
  outline: none;
  width: 100%;
  position: relative;
  box-sizing: border-box;

  &:focus ~ ${BorderBottom} {
    background-color: #4285f4;
    width: 100%;
  }
`;

const CreateTodoButton = styled.button`
  font-size: 16px;
  font-weight: bold;
  border: none;
  border-radius: 8px;
  outline: none;
  cursor: pointer;
  margin-left: 8px;
  width: 20%;
  background-color: #4169e1;
  color: #ffffff;

  &:hover {
    opacity: 0.8;
  }

  @media (max-width: 767px) {
    padding: 6px;
    width: auto;
    margin-top: 10px;
    margin-left: 0;
  }
`;

const NewTodoForm = ({ todos, onCreate }) => {
  const [inputValue, setInputValue] = useState('');
  const submitTodo = (e) => {
    e.preventDefault();
    const cleanTrim = inputValue.trim();
    if (cleanTrim === '') {
      alert(`Can't empty!`);
      return;
    } else {
      const isDuplicate = todos.some((todo) => todo.text === cleanTrim);
      if (!isDuplicate) {
        onCreate(cleanTrim);
        setInputValue('');
      } else {
        alert(`Duplicate text "${cleanTrim}"`);
      }
    }
  };

  return (
    <CreateTodoForm onSubmit={submitTodo}>
      <InputContainer>
        <InputTodo
          type='text'
          value={inputValue}
          placeholder='Type your todo'
          autoFocus
          onChange={(e) => setInputValue(e.target.value)}
        />
        <BorderBottom />
      </InputContainer>
      <CreateTodoButton type='submit'>Create</CreateTodoButton>
    </CreateTodoForm>
  );
};

const mapStateToProps = (state) => ({
  todos: getTodos(state),
});

const mapDispatchToProps = {
  onCreate: createTodo,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewTodoForm);
