import React from 'react';
import TodoItem from './TodoItem';
import styled from 'styled-components/macro';
import { connect } from 'react-redux';
import { getTodos, getFilters } from '../selectors/selectors';
import { removeTodo, completeTodo } from '../redux/action';

const TodoListContainer = styled.ul`
  list-style-type: none;
  padding-left: 0;
  margin-top: 0;
`;

const TodoList = ({ allTodos = [], onDelete, onComplete, filterTodo }) => {
  const filterType = {
    all: () => true,
    active: (todoPer) => !todoPer.isCompleted,
    completed: (todoPer) => todoPer.isCompleted,
  };

  const listTodo = allTodos
    .filter(filterType[filterTodo])
    .map((todoloop) => (
      <TodoItem
        key={todoloop.id}
        todoPerItem={todoloop}
        onDelete={onDelete}
        onComplete={onComplete}
        isCompleted={todoloop.isCompleted}
      />
    ))
    .reverse();

  return <TodoListContainer>{listTodo}</TodoListContainer>;
};

const mapStateToProps = (state) => ({
  allTodos: getTodos(state),
  filterTodo: getFilters(state),
});

const mapDispatchToProps = {
  onDelete: removeTodo,
  onComplete: completeTodo,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
