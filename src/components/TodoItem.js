import React, { useState, useRef, useEffect } from 'react';
import { editTodo } from '../redux/action';
import { connect } from 'react-redux';
import styled from 'styled-components/macro';

const TodoItemContainer = styled.div`
  background-color: ${(props) =>
    props.$background ? 'rgb(223, 240, 216)' : 'rgb(242, 222, 222)'};
  border-radius: 8px;
  margin-top: 8px;
  padding: 10px;
  box-shadow: 0 4px 8px grey;
  transition: all 0.5s;

  &:hover {
    background-color: #d3d3d3;
  }
`;

const TodoText = styled.p`
  text-decoration: ${(props) => (props.$linethrough ? 'line-through' : null)};
`;

const TimeAndAction = styled.div`
  display: flex;
  justify-content: space-between;

  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

const Time = styled.p`
  margin: 0;
  align-self: center;

  @media (max-width: 767px) {
    align-self: auto;
    text-align: right;
    margin-bottom: 10px;
  }
`;

const TimeDetail = styled.time`
  font-size: 13px;
`;

const ButtonsContainer = styled.div`
  text-align: right;
`;

const Button = styled.button`
  font-size: 15px;
  font-weight: 700;
  padding: 6px;
  border: none;
  border-radius: 8px;
  outline: none;
  cursor: pointer;
`;

const CompletedButton = styled(Button)`
  display: inline-block;
  background-color: #5cb85c;

  &:hover {
    background-color: #47a447;
  }
`;

const DeleteButton = styled(Button)`
  background-color: #d9534f;
  margin-left: 8px;

  &:hover {
    background-color: #cf3341;
  }
`;

const EditButton = styled(Button)`
  background-color: #20b2aa;
  margin-left: 8px;

  &:hover {
    opacity: 0.8;
  }
`;

const EditTodoContainer = styled.form`
  display: flex;
  justify-content: space-between;
  border-radius: 8px;
  margin-top: 8px;
  padding: 10px;
  box-shadow: 0 4px 8px grey;
  transition: all 0.5s;

  @media (max-width: 480px) {
    flex-direction: column;
  }
`;

const CancelButton = styled(Button)`
  background-color: #d3d3d3;
  margin-left: 8px;

  &:hover {
    opacity: 0.8;
  }

  @media (max-width: 480px) {
    width: 50%;
    margin-left: 0;
  }
`;

const SaveButton = styled(Button)`
  background-color: #ff1493;
  margin-left: 8px;

  &:hover {
    opacity: 0.8;
  }

  @media (max-width: 480px) {
    width: 50%;
    margin-left: 0;
  }
`;

const EditButtonContainer = styled.div`
  @media (max-width: 480px) {
    margin-top: 10px;
  }
`;

const EditTodoText = styled.input`
  width: 60%;
  padding: 5px 0 5px 5px;
  box-sizing: border-box;

  @media (max-width: 480px) {
    width: 100%;
  }
`;

const TodoItem = ({
  isCompleted,
  todoPerItem,
  onDelete,
  onComplete,
  onEdit,
}) => {
  const [isEditing, setEditing] = useState(false);
  const [newText, setNewText] = useState(todoPerItem.text);
  const editInputRef = useRef(null);

  const submitEdit = (e) => {
    e.preventDefault();
    onEdit(todoPerItem.id, newText);
    setEditing(false);
  };

  const viewTodoItem = (
    <TodoItemContainer $background={isCompleted}>
      {todoPerItem.isCompleted ? (
        <TodoText $linethrough='true'>{todoPerItem.text}</TodoText>
      ) : (
        <TodoText>{todoPerItem.text}</TodoText>
      )}
      <TimeAndAction>
        <Time>
          <TimeDetail
            dateTime={new Date(todoPerItem.createdAt).toLocaleDateString()}
          >
            Created at {new Date(todoPerItem.createdAt).toLocaleTimeString()}
          </TimeDetail>
        </Time>
        <ButtonsContainer>
          {todoPerItem.isCompleted ? null : (
            <CompletedButton
              onClick={() => {
                onComplete(todoPerItem.id);
              }}
            >
              Completed
            </CompletedButton>
          )}
          {todoPerItem.isCompleted ? null : (
            <EditButton onClick={() => setEditing(true)}>Edit</EditButton>
          )}
          <DeleteButton
            onClick={() => {
              const decide = window.confirm('Are you want to delete?');
              if (decide) {
                onDelete(todoPerItem.id);
              }
            }}
          >
            Delete
          </DeleteButton>
        </ButtonsContainer>
      </TimeAndAction>
    </TodoItemContainer>
  );

  const editTodoItem = (
    <EditTodoContainer onSubmit={submitEdit}>
      <EditTodoText
        type='text'
        value={newText}
        ref={editInputRef}
        onChange={(e) => setNewText(e.target.value)}
      ></EditTodoText>
      <EditButtonContainer>
        <SaveButton type='submit'>Save</SaveButton>
        <CancelButton
          onClick={() => {
            setEditing(false);
            setNewText(todoPerItem.text);
          }}
        >
          Cancel
        </CancelButton>
      </EditButtonContainer>
    </EditTodoContainer>
  );

  useEffect(() => {
    if (isEditing) {
      editInputRef.current.focus();
    }
  }, [isEditing]);

  // Ensure edit field data correspond with todo text data
  useEffect(() => {
    setNewText(todoPerItem.text);
  }, [todoPerItem.text]);

  return <li>{isEditing ? editTodoItem : viewTodoItem}</li>;
};

const mapDispatchToProps = {
  onEdit: editTodo,
};

export default connect(null, mapDispatchToProps)(TodoItem);
