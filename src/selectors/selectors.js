import { createSelector } from 'reselect';

export const getTodos = (state) => state.todos;

export const getFilters = (state) => state.filters;

export const getIncompleteTodos = createSelector(getTodos, (todos) =>
  todos.filter((todoPer) => !todoPer.isCompleted),
);

export const getCompletedTodos = createSelector(getTodos, (todos) =>
  todos.filter((todoPer) => todoPer.isCompleted),
);
